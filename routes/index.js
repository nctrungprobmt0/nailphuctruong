const express = require('express');
const router = express.Router();
const auth = require('../middlewares/auth');

const setup = require('./../controllers/setup');
const DashboardCtrl = require('./../controllers/admin/dashboard');

// router.get('/setup', setup.setup);
router.get('/dashboard', auth, DashboardCtrl.index)


module.exports = app => {
    app.use('/administrator', router);
  
    app.get('*', (req, res) => {
      res.status(404).json({ message: 'not found1' });
    });
  
    app.use((err, req, res, next) => {
      if (err.type === 'entity.parse.failed') {
        return res.status(400).json({ message: 'bad request' });
      }
      next(err);
    });
  };