'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');
var info = {
    socket: null,
    io: null,
    get sockets() {
        return {
        	socket : this.socket,
        	io : this.io
        };
    },
    set sockets (infoSocket) {
        this.socket = infoSocket[0] || null;
        this.io = infoSocket[1] || null;
    }
}
const Voucherchema = new Schema({
	start_date : { type: Date, default: "" },
    end_date : { type: Date, default: "" },
	status : Number,
    number : Number,
    name :  String,
    point : Number,
    created: { type: Date, default: "" }

})
.post('update', function (doc) {
	
})
.post('save', function (doc) {
	
})
.post('remove', function (doc) {
	
})
;
return module.exports = {
	infoSocket : function(socket, io){
		info.sockets = [socket, io];
	},
	module : function(){
		return mongoose.model('Voucher', Voucherchema);
	}
}