'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');
var info = {
    socket: null,
    io: null,
    get sockets() {
        return {
        	socket : this.socket,
        	io : this.io
        };
    },
    set sockets (infoSocket) {
        this.socket = infoSocket[0] || null;
        this.io = infoSocket[1] || null;
    }
}
const Bookingchema = new Schema({
	services : String,
	time_book : String,
	time_min_book : String,
	date_book : String,
	fullname : String,
	telephone : String,
	status : Number,
	online : Number, //1 den tiem, 0 pending
	note : String,
	queue : Number,
	bill: {type: String, default: '0'},
	personnel: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Personnel'
    },
    rating: String,
    created: { type: Date, default: "" }

})
.post('updateOne', function (doc) {

	(parseInt(this._update.$set.online) == 1 || parseInt(this._update.$set.online) == 0) && (
		(parseInt(this._update.$set.online) == 1) ? (

			this.findOne({'_id' : this._conditions._id}).populate('personnel',{'fullname' : 1,'telephone' : 1}).exec(function(err,result) {

				(!err && result && info.sockets.socket) && (
					info.sockets.socket.broadcast.emit('Booking:CreateOnline', result),
					info.sockets.socket.emit('Booking:CreateOnline', result)
				)
			})
		) : (
			info.sockets.socket.broadcast.emit('Booking:RemoveOnline', this._conditions._id),
			info.sockets.socket.emit('Booking:RemoveOnline', this._conditions._id)
		)
	),

	(parseInt(this._update.$set.status) == 2 && info.sockets.socket) && (
		
		info.sockets.socket.broadcast.emit('Booking:RemoveOnline', this._conditions._id),
		info.sockets.socket.emit('Booking:RemoveOnline', this._conditions._id)
	)
	
})
.post('save', function (doc) {
	(doc.queue == 0 && info.sockets.socket) && (
		info.sockets.socket.broadcast.emit('Booking:save', doc),
		info.sockets.socket.emit('Booking:save', doc)
	)
})
.post('remove', function (doc) {
	
})
;
return module.exports = {
	infoSocket : function(socket, io){
		info.sockets = [socket, io];
	},
	module : function(){
		return mongoose.model('Booking', Bookingchema);
	}
}