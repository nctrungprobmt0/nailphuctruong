'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');
var info = {
    socket: null,
    io: null,
    get sockets() {
        return {
        	socket : this.socket,
        	io : this.io
        };
    },
    set sockets (infoSocket) {
        this.socket = infoSocket[0] || null;
        this.io = infoSocket[1] || null;
    }
}
const Ratingchema = new Schema({
	type : String,
    description : String,
    option : [],
	status : Number,
    booking_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Booking'
    },
    personnel_id : {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Personnel'
    },
    created: { type: Date, default: "" },
    rate_num: {type: String, default: 0},
	rate_avg: {type: String, default: 0}

})
.post('update', function (doc) {
	
})
.post('save', function (doc) {
	
})
.post('remove', function (doc) {
	
})
;
return module.exports = {
	infoSocket : function(socket, io){
		info.sockets = [socket, io];
	},
	module : function(){
		return mongoose.model('Rating', Ratingchema);
	}
}