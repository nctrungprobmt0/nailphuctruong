'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');
var info = {
    socket: null,
    io: null,
    get sockets() {
        return {
        	socket : this.socket,
        	io : this.io
        };
    },
    set sockets (infoSocket) {
        this.socket = infoSocket[0] || null;
        this.io = infoSocket[1] || null;
    }
}
const Personnelchema = new Schema({
	fullname : String,
	telephone : String,
	birthday : String,
	address : String,
	salary : String,
	date_work : String,
	point: {type:Number, default: 30},
	advance_salary:{type:String, default: '0'},
	status : Number,
	rate_num: {type: String, default: 0},
	rate_avg: {type: String, default: 0},
    created: { type: Date, default: "" }

})
.post('update', function (doc) {
	
})
.post('save', function (doc) {
	
})
.post('remove', function (doc) {
	
})
;
return module.exports = {
	infoSocket : function(socket, io){
		info.sockets = [socket, io];
	},
	module : function(){
		return mongoose.model('Personnel', Personnelchema);
	}
}