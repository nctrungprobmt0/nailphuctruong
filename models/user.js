var mongoose = require('mongoose');  
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');
var UserSchema = new mongoose.Schema({  
	fullname: { type: String },
	username: { type: String },
	password: { type: String },
    status: { type: String, default: '0'},
    created: { type: Date, default: "" }
});


UserSchema.post('save', function (doc) {
    
});

UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};


UserSchema.methods.validPassword = function(password) {
	let user = this
    return bcrypt.compareSync(password, user.password);
};

module.exports = mongoose.model('User',UserSchema);