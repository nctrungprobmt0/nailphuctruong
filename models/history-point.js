'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');
var info = {
    socket: null,
    io: null,
    get sockets() {
        return {
        	socket : this.socket,
        	io : this.io
        };
    },
    set sockets (infoSocket) {
        this.socket = infoSocket[0] || null;
        this.io = infoSocket[1] || null;
    }
}
const HistoryPointchema = new Schema({
	member: {
      	type: mongoose.Schema.Types.ObjectId,
      	ref: 'Member'
    },
	point : Number,
	type : String,
    created: { type: Date, default: "" },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    description : String
})
.post('update', function (doc) {
	
})
.post('save', function (doc) {
	
})
.post('remove', function (doc) {
	
})
;
return module.exports = {
	infoSocket : function(socket, io){
		info.sockets = [socket, io];
	},
	module : function(){
		return mongoose.model('Historypoint', HistoryPointchema);
	}
}