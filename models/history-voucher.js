'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');
var info = {
    socket: null,
    io: null,
    get sockets() {
        return {
        	socket : this.socket,
        	io : this.io
        };
    },
    set sockets (infoSocket) {
        this.socket = infoSocket[0] || null;
        this.io = infoSocket[1] || null;
    }
}
const HistoryVoucherchema = new Schema({
	member: {
      	type: mongoose.Schema.Types.ObjectId,
      	ref: 'Member'
    },
	description : String,
	type : String,
    created: { type: Date, default: "" },
    voucher: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Voucher'
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },

})
.post('update', function (doc) {
	
})
.post('save', function (doc) {
	
})
.post('remove', function (doc) {
	
})
;
return module.exports = {
	infoSocket : function(socket, io){
		info.sockets = [socket, io];
	},
	module : function(){
		return mongoose.model('Historyvoucher', HistoryVoucherchema);
	}
}