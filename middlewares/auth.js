'use strict'
const jwt  = require('jsonwebtoken');
var config = require('../config');
const User = require('../models/user');
function isAuth(req,res,next){

	//req.session.userId = '5cfc8a464e2ead3c3257f9c4';

	!req.session.userId ? res.redirect('/administrator/login') :
	User.findById(req.session.userId, function(err, user) {
        req.user = user
		next()
    })
}

module.exports = isAuth;