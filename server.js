var app = require('./app');
var port = process.env.PORT || 8081;
var express = require('express');



var http = require('http').Server(app);
var io = require('socket.io')(http);


require('./socket/index.js')(io);

/*io.on('connection', function(socket){
  	console.log('a user connected');
});*/

const config = require('./config');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

mongoose.connect(config.db,  { useNewUrlParser: true ,useCreateIndex : true})
	.then(() => {
		http.listen(config.port, () =>{

			console.log(`Server Work http://localhost:${config.port}`);
		});
	})
	.catch(err => console.error(`Error al conectar a la DB: ${err}`));


module.exports = io;