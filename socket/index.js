// 'use strict';
function onDisconnect(socket) {
}

function onConnect(socket,io) {
	socket.on('info', function (data) {
		console.info('[%s] %s', socket.address, JSON.stringify(data, null, 2));
	});
	/*socket.on('Post:typing-commnet',function(data){
  		socket.broadcast.emit('Post:typing-commnet',data);
  	})
  	socket.on('Post:typing-commnet-child',function(data){
  		socket.broadcast.emit('Post:typing-commnet-child',data);
  	})*/
	require('../models/booking').infoSocket(socket, io);
	require('../controllers/cron').infoSocke(socket, io);
	/*require('../controllers/post').infoSocket(socket, io);
	require('../controllers/notification').infoSocket(socket, io);
	require('../models/notification').infoSocket(socket, io);
	require('../controllers/socket').infoSocket(socket, io);*/
}

module.exports = function (io) {
  	
  	io.on('connection', function (socket) {
  		//console.log("user connect socket");
	    socket.on('disconnect', function () {
	      	onDisconnect(socket);
	    });
	    onConnect(socket,io);
  	});


};