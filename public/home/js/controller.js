'use strict';

var app = angular.module('myApp', ['ui.bootstrap'])
.filter('slice', function() {
  return function(arr, start, end) {
    return (arr || []).slice(start, end);
  };
}).factory('socketio', ['$rootScope' , function ($rootScope) {
       
    
    var socket = io.connect(host,{ 'forceNew': true });
    
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        }
    };

}]);

app.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{');
  $interpolateProvider.endSymbol('}]}');
});

app.controller('CtrMemberOnline', ['$scope','$http','socketio', function($scope,$http,socketio) {
    //var result = [];
    $scope.data = [];
    $http({
        url: "/administrator/booking/get-online",
        dataType: "json",
        method: "GET",
        data: {},
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        }
    }).success(function(response){  
        
        $scope.data = response;
        
    }).error(function(error){
        $scope.error = error;
    });

    


    socketio.on('Booking:RemoveOnline', (data)=>{
        
        for (var s = 0; s < $scope.data.length; s++) {
            if ($scope.data[s]._id == data)
            {
                $scope.data.splice(s, 1);
                break;
            }
        };
        
    });

    socketio.on('Booking:CreateOnline', (data)=>{
        $scope.data.push(data);
        
    });

    $scope.Click_Rating = function(booking_id,personnel_id) {
        $('input[name="booking_id"]').val(booking_id);
        $('input[name="personnel_id"]').val(personnel_id);
        $('.show_one').show();
        $('.online_member').hide();
        $('.fa-arrow-circle-left').show();
        
    }

    
}]);


