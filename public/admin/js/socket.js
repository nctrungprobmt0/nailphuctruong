'use strict';
$(function() {
   
    load_home();
    var socket = io.connect(host,{ 'forceNew': true })
    
    socket.on('connect', function(){
        console.log('connect socket');
        //$('.ws-status-icon i').css({'color' : '#468847'});
    });
   
    socket.on('disconnect', function(){
        console.log('disconnect socket')
    });
    
    
    socket.on('Booking:save', function (data) {
        var table_book_pendding = $('#table_book_pendding').DataTable();
        var trDOM = table_book_pendding.row.add( [
            formatAMPM(data.created),
            data.fullname,
            data.telephone,
            data.date_book + ' ' + data.time_book,
            data.services,
            '<div class="list-icons"> <div class="dropdown"> <a href="#" class="list-icons-item" data-toggle="dropdown"> <i class="icon-menu9"></i> </a> <div class="dropdown-menu dropdown-menu-right"> <a href="#" data-id="'+data._id+'" data-index="'+table_book_pendding.rows().data().length+'" class="dropdown-item cancel_booking_pendding"><i class="icon-cancel-circle2"></i> Hủy lịch hẹn</a> <a href="#" data-id="'+data._id+'" data-index="'+table_book_pendding.rows().data().length+'" class="dropdown-item edit_booking_pendding"><i class="icon-pencil7"></i> Sửa đặt hẹn</a> </div> </div> </div>'
        ] ).draw( true ).node();
        $(trDOM).addClass('addclass-row');
        document.getElementById("notification_sound").play();
        load_home();
        setTimeout(function() {
            $(trDOM).removeClass('addclass-row');
        }, 3000);
    });

    socket.on('Booking:RemoveOnline', function (data) {
        var table_book_confirm = $('#table_book_confirm').DataTable();
        table_book_confirm.row("#"+data).remove().draw();
        console.log(data);
    })
    
    socket.on('Booking:Today', function (data) {
        var html = "";
    
        for(const booking of data){
    
            if(parseFloat(booking.c_minutes) % 5 == 0 && parseFloat(booking.c_minutes) > 0 ){
                document.getElementById("lichhen_sound").play();
            } 
            html += `<tr>
                <td> <div class="d-flex align-items-center">
                                      
            <div>
                <a href="#" class="text-default font-weight-semibold">${booking.fullname}</a>
                <div class="text-muted font-size-sm">
                    ${booking.telephone}
                </div>
            </div>
        </div></td>
            <td>${booking.time_book}</td>
            <td>${booking.services}</td>
           
            </tr>`;
        }
        $( "#upBooking" ).html( html );
        // console.log(data)
        // var table_book_confirm = $('#table_book_confirm').DataTable();
        // table_book_confirm.row("#"+data).remove().draw();
        // console.log(data);
    })
    socket.on('Birthday:List', function (data) {
        var html = "";
        // document.getElementById("sinhnhat_sound").play();
        for (const member of data){
            if(parseFloat(member.c_hours) == -8 && member.c_minutes == 0 ){
                document.getElementById("lichhen_sound").play();
            } 
            html += `
            <li class="media"> <div class="media-body" >Hôm nay là sinh nhật của <a href="/profile/${member.telephone}"> ${member.fullname} (${member.telephone})</a></div> </li>
            `;
        }
        $( "#birthDayList" ).html( html );
        // var table_book_confirm = $('#table_book_confirm').DataTable();
        // table_book_confirm.row("#"+data).remove().draw();
        // console.log(data);
    })

    /*socket.emit('Create-Room-Exchange', name_coin+'-'+exchange);
    socket.on('connect_failed', function (data) {
        console.log(data || 'connect_failed');
    });*/
    
 
})
function formatAMPM(date) {
    var date = new Date(date);
    
    var dd = date.getDate();
    var mm = date.getMonth() + 1; 

    var yyyy = date.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    } 
    if (mm < 10) {
      mm = '0' + mm;
    } 
    var hours = date.getHours();
    var minutes = date.getMinutes();
    hours = hours < 10 ? '0'+hours : hours;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = dd+'/'+mm+'/'+yyyy+' '+hours + ':' + minutes;
    return strTime;
}
function load_home(){
    function ValidateUSPhoneNumber(phoneNumber) {
      var regExp = /(^0[0-9]{9,10}$)/;
      return regExp.test(String(phoneNumber));
    }
    $('#booking_nail').on('submit',function(){
        $('#booking_nail .alert-danger').hide();
        $('#booking_nail .fa-spinner').show();
        $('#booking_nail .site-button').attr('disabled', 'disabled');
        
        var services = $('#booking_nail select[name="services"]').val();
        var time_book = $('#booking_nail *[name="time_book"]').val();
        var date_book = $('#booking_nail input[name="date_book"]').val();
        var fullname = $('#booking_nail input[name="fullname"]').val();
        var telephone = $('#booking_nail input[name="telephone"]').val();
        var token = $('#booking_nail input[name="token"]').val();
        var queue = $('#booking_nail select[name="queue"]').val();
        var personnel = $('#booking_nail select[name="personnel"]').val();
        if (services == '')
        {
            $('#booking_nail .alert-danger').show().html('Bạn vui lòng chọn dịch vụ.');
            $('#booking_nail .fa-spinner').hide();
                $('#booking_nail .site-button').removeAttr('disabled');
            return false;
        }
        if (personnel == '')
        {
            $('#booking_nail .alert-danger').show().html('Bạn vui lòng chọn nhân viên.');
            $('#booking_nail .fa-spinner').hide();
                $('#booking_nail .site-button').removeAttr('disabled');
            return false;
        }
        
        if (time_book == '')
        {
            $('#booking_nail .alert-danger').show().html('Bạn vui lòng chọn thời gian đặt lịch.');
            $('#booking_nail .fa-spinner').hide();
                $('#booking_nail .site-button').removeAttr('disabled');
            return false;
        }
        if (date_book == '')
        {
            $('#booking_nail .alert-danger').show().html('Bạn vui lòng chọn ngày tháng đặt lịch.');
            $('#booking_nail .fa-spinner').hide();
                $('#booking_nail .site-button').removeAttr('disabled');
            return false;
        }
        if (fullname == '')
        {
            $('#booking_nail .alert-danger').show().html('Bạn vui lòng nhập họ tên.');
            $('#booking_nail .fa-spinner').hide();
                $('#booking_nail .site-button').removeAttr('disabled');
            return false;
        }
        if (telephone == '' || !ValidateUSPhoneNumber(telephone))
        {
            $('#booking_nail .alert-danger').show().html('Bạn vui lòng nhập số điện thoại.');
            $('#booking_nail .fa-spinner').hide();
                $('#booking_nail .site-button').removeAttr('disabled');
            return false;
        }
        
        $.ajax({
            url : "/administrator/booking/create-booking-admin",
            type : "post",
            dataType:"text",
            data : {
                'services' : services,
                'time_book' : time_book,
                'date_book' : date_book,
                'fullname' : fullname,
                'telephone' : telephone,
                'token' : token,
                'queue' : queue,
                'personnel' : personnel
            },
            success : function (result){
                location.reload();
                var data = $.parseJSON(result)
                $('#booking_nail input[name="token"]').val(data.token);
                $('#booking_nail .alert-success').show().html('Bạn đã đặt lịch thành công!');
                $('#booking_nail .fa-spinner').hide();
                $('#booking_nail .site-button').removeAttr('disabled');
                /*setTimeout(function() {

                    if (queue == 1) 
                    {
                        var table_book_confirm = $('#table_book_confirm').DataTable();
                        table_book_confirm.row.add( [
                            table_book_confirm.rows().data().length + 1,
                            fullname,
                            telephone,
                            date_book + ' ' + time_book,
                            '<div class="list-icons"> <div class="dropdown"> <a href="#" class="list-icons-item" data-toggle="dropdown"> <i class="icon-menu9"></i> </a> <div class="dropdown-menu dropdown-menu-right"> <a href="#" class="dropdown-item"><i class="icon-checkbox-checked"></i> Xác nhận đặt lịch</a> <a href="#" class="dropdown-item"><i class="icon-cancel-circle2"></i> Hủy lịch hẹn</a> <a href="#" class="dropdown-item"><i class="icon-pencil7"></i> Sửa đặt hẹn</a> </div> </div> </div>'
                        ] ).draw( false );
                    }
                    
                    $('#modal_iconified').modal('hide');



                }, 500);*/
            },
            error : function (xhr, ajaxOptions, thrownError){
                $('#booking_nail .fa-spinner').hide();
                $('#booking_nail .site-button').removeAttr('disabled');
                $('#booking_nail .alert-danger').show().html('Error');
                
                
            }
        });

        
        return false;
    })


    $('.edit_booking_pendding').on('click',function(){
        var id = $(this).data('id');
        var index = $(this).data('index');
        $.ajax({
            url : "/administrator/booking/get-booking-id",
            type : "post",
            dataType:"text",
            data : {
                'id' : id
            },
            success : function (result){
                var data = $.parseJSON(result)
                
                $('#edit_booking_nail select[name="services"]').val(data.services);
                $('#edit_booking_nail *[name="time_book"]').val(data.time_book);
                $('#edit_booking_nail input[name="date_book"]').val(data.date_book);
                $('#edit_booking_nail input[name="fullname"]').val(data.fullname);
                $('#edit_booking_nail input[name="telephone"]').val(data.telephone);
                $('#edit_booking_nail input[name="id"]').val(data._id);
                $('#edit_booking_nail input[name="index"]').val(index);
                $('#edit_booking_nail select[name="queue"]').val(data.queue);
                $('#edit_booking_nail select[name="personnel"]').val(data.personnel);
                $('#modal_edit_booking_modal').modal('show');
                
            }
        })
        return false;
    })


    $('#edit_booking_nail').on('submit',function(){

        $('#edit_booking_nail .alert-danger').hide();
        $('#edit_booking_nail .fa-spinner').show();
        $('#edit_booking_nail .site-button').attr('disabled', 'disabled');
        var services = $('#edit_booking_nail select[name="services"]').val();
        var time_book = $('#edit_booking_nail *[name="time_book"]').val();
        var date_book = $('#edit_booking_nail input[name="date_book"]').val();
        var fullname = $('#edit_booking_nail input[name="fullname"]').val();
        var telephone = $('#edit_booking_nail input[name="telephone"]').val();
        var id = $('#edit_booking_nail input[name="id"]').val();
        var index = $('#edit_booking_nail input[name="index"]').val();
        var queue = $('#edit_booking_nail select[name="queue"]').val();
        var personnel = $('#edit_booking_nail select[name="personnel"]').val();

        if (time_book == '')
        {
            $('#edit_booking_nail .alert-danger').show().html('Bạn vui lòng chọn thời gian đặt lịch.');
            $('#edit_booking_nail .fa-spinner').hide();
                $('#edit_booking_nail .site-button').removeAttr('disabled');
            return false;
        }
        if (personnel == '')
        {
            $('#booking_nail .alert-danger').show().html('Bạn vui lòng chọn nhân viên.');
            $('#booking_nail .fa-spinner').hide();
                $('#booking_nail .site-button').removeAttr('disabled');
            return false;
        }
        if (date_book == '')
        {
            $('#edit_booking_nail .alert-danger').show().html('Bạn vui lòng chọn ngày tháng đặt lịch.');
            $('#edit_booking_nail .fa-spinner').hide();
                $('#edit_booking_nail .site-button').removeAttr('disabled');
            return false;
        }
        if (fullname == '')
        {
            $('#edit_booking_nail .alert-danger').show().html('Bạn vui lòng nhập họ tên.');
            $('#edit_booking_nail .fa-spinner').hide();
                $('#edit_booking_nail .site-button').removeAttr('disabled');
            return false;
        }
        if (telephone == '' || !ValidateUSPhoneNumber(telephone))
        {
            $('#edit_booking_nail .alert-danger').show().html('Bạn vui lòng nhập số điện thoại.');
            $('#edit_booking_nail .fa-spinner').hide();
                $('#edit_booking_nail .site-button').removeAttr('disabled');
            return false;
        }
        if (id == '')
        {
            $('#edit_booking_nail .alert-danger').show().html('Error NetWork.');
            $('#edit_booking_nail .fa-spinner').hide();
                $('#edit_booking_nail .site-button').removeAttr('disabled');
            location.reload();
            return false;
        }

        $.ajax({
            url : "/administrator/booking/edit-booking-by-id",
            type : "POST",
            dataType:"text",
            data : {
                'services' : services,
                'time_book' : time_book,
                'date_book' : date_book,
                'fullname' : fullname,
                'telephone' : telephone,
                'id' : id,
                'queue' : queue,
                'personnel' : personnel
            },
            success : function (result){
                location.reload();
                $('#edit_booking_nail .alert-success').show().html('Bạn đã sửa đặt lịch thành công!');
                $('#edit_booking_nail .fa-spinner').hide();
                $('#edit_booking_nail .site-button').removeAttr('disabled');
                /*setTimeout(function() {
                    if (queue == 0)
                    {
                        var table = $('#table_book_pendding').DataTable();
                        
                        var temp = table.row(index).data();
                        temp[0] = fullname;
                        temp[1] = telephone;
                        temp[2] = date_book+' '+time_book;
                        
                        table.row(index).data(temp).draw();
                    }
                    else
                    {
                        $('#table_book_pendding').DataTable().row(index).remove().draw();
                        var table_book_confirm = $('#table_book_confirm').DataTable();
                        table_book_confirm.row.add( [
                            table_book_confirm.rows().data().length + 1,
                            fullname,
                            telephone,
                            date_book + ' ' + time_book,
                            '<div class="list-icons"> <div class="dropdown"> <a href="#" class="list-icons-item" data-toggle="dropdown"> <i class="icon-menu9"></i> </a> <div class="dropdown-menu dropdown-menu-right"> <a href="#" class="dropdown-item"><i class="icon-checkbox-checked"></i> Xác nhận đặt lịch</a> <a href="#" class="dropdown-item"><i class="icon-cancel-circle2"></i> Hủy lịch hẹn</a> <a href="#" class="dropdown-item"><i class="icon-pencil7"></i> Sửa đặt hẹn</a> </div> </div> </div>'
                        ] ).draw( false );
                    }

                    $('#modal_edit_booking_modal').modal('hide');

                }, 500);*/
            },
            error : function (xhr, ajaxOptions, thrownError){
                $('#edit_booking_nail .fa-spinner').hide();
                $('#edit_booking_nail .site-button').removeAttr('disabled');
                $('#edit_booking_nail .alert-danger').show().html('Error');
                
                
            }
        });

        return false;
    })
    
    $('.cancel_booking_pendding').on('click',function(){
        var id = $(this).data('id');
        var index = $(this).data('index');
        if(confirm('Bạn có chắc chắn với lựa chọn của mình ?'))
        {
            $.ajax({
                url : "/administrator/booking/cancel-booking-by-id",
                type : "POST",
                dataType:"text",
                data : {
                    'id' : id
                },
                success : function (result){
                    location.reload();
                    $('#table_book_pendding').DataTable().row(index).remove().draw();
                }
            })
        }
    })


    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
      }
    $('#confirm_bill input[name="amount"]').on('input propertychange',function(){
        $(this).val(formatNumber($(this).val().replace(/,/g, ''))); 
    })

    $('.confirm_booking').on('click',function(){

        var id = $(this).data('id');
        $('#confirm_bill input[name="id"]').val(id);
        $("#modal_confirm_bill").modal()
        return false;
        var index = $(this).data('index');
        if(confirm('Bạn có chắc chắn với lựa chọn của mình ?'))
        {
            $.ajax({
                url : "/administrator/booking/confirm-booking-by-id",
                type : "POST",
                dataType:"text",
                data : {
                    'id' : id
                },
                success : function (result){
                    location.reload();
                }
            })
        }
    })

    $('#confirm_bill').on('submit',function(e){
        e.preventDefault();
        var amount = $('#confirm_bill input[name="amount"]').val().replace(/,/g, '');
        if (amount == '' || isNaN(amount))
        {
            $('#confirm_bill .alert-danger').show().html('Bạn vui lòng nhập lương cơ bản.');
            $('#confirm_bill .fa-spinner').hide();
            $('#confirm_bill .site-button').removeAttr('disabled');
            return false;
        }
        if(confirm('Bạn có chắc chắn với lựa chọn của mình ?'))
        {
            $.ajax({
                url :  "/administrator/booking/confirm-booking-by-id",
                type: 'POST',
                data : {
                    'id' : $('#confirm_bill input[name="id"]').val(),
                    'amount': amount
                },
            }).done(function(response){
                location.reload();
            }).fail((err) =>{
                $('#confirm_bill .alert-danger').show().html(err.responseJSON.message);
            });


        }
    });
    

    

}