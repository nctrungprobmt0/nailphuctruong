let secondlyCron = '0-59/5 * * * * *';
let minutelyCron = '* * * * *';
let dailyCron = '0 17 8 * * *';
let weeklyCron = '0 48 7 * * 7';
const CronJob = require('cron').CronJob
const moment = require('moment');
const User = require('../../models/user');
const Member = require('../../models/member').module();
const Booking = require('../../models/booking').module();

const Personnel = require('../../models/personnel').module();
process.env.TZ = 'Asia/Ho_Chi_Minh'
var info = {
    socket: null,
    io: null,
    get sockets() {
        return {
            socket : this.socket,
            io : this.io
        };
    },
    set sockets (infoSocket) {
        this.socket = infoSocket[0] || null;
        this.io = infoSocket[1] || null;
    }
}

thongBaoSinhNhatKhach = async () =>
{
   try
   {
      console.log('sn')
      let members = await Member.find({})
      let today = moment()
      today =  moment(today).format()
      var year = moment().year();
      let toDayBirthday= [];
      let upcomingBirthdays = [];
      for (var member of members)
      {
          member = JSON.parse(JSON.stringify(member))
         var parts = member.birthday.split("/");
         var parts_date = `${year}-${parseInt(parts[1], 10)}-${parseInt(parts[0], 10)}`
         var dt = new Date(parts_date);
         let birthday = moment(dt);
         let c_days = birthday.diff(today, 'days');
         let c_hours = birthday.subtract(c_days, 'days').diff(today, 'hours');
         let c_minutes = birthday.subtract(c_hours, 'hours').diff(today, 'minutes');
         console.log('sinhnhatm',c_days, c_hours, c_minutes);

         if(c_days >= 0 && c_days <= 7 && c_hours <= 0){
             member.birthday = `${parseInt(parts[0], 10)}/${parseInt(parts[1], 10)}/${year}`
            toDayBirthday.push({...member, c_hours: c_hours, c_minutes: c_minutes})
         }

      }
        (info.sockets.socket && toDayBirthday.length > 0) && (
            info.sockets.socket.emit('Birthday:List', toDayBirthday),
            info.sockets.socket.broadcast.emit('Birthday:List', toDayBirthday)
        )
   }
   catch (error)
   {
      console.log(error)
   }
}
// thongBaoSinhNhat()

nhacLichHenKhach = async () =>
{
   try {
      console.log('lich')
      let bookings = await Booking.find({status: 0})
      let today = moment()
      today =  moment(today).format()
      let bookingToday = []
      for (var booking of bookings) {
         booking = JSON.parse(JSON.stringify(booking))
         var parts = booking.date_book.split("/");
         var dt = new Date(parseInt(parts[2], 10), parseInt(parts[1], 10) - 1, parseInt(parts[0], 10));
         let days = moment(dt).format('YYYY-MM-DD');
         let time_book = days + " " + booking.time_book + ':00';
         let dateTimeBook = moment(new Date(time_book)).format();
         dateTimeBook = moment(dateTimeBook)

         let c_days = dateTimeBook.diff(today, 'days');
         let c_hours = dateTimeBook.subtract(c_days, 'days').diff(today, 'hours');
         let c_minutes = dateTimeBook.subtract(c_hours, 'hours').diff(today, 'minutes');
        //  console.log(c_days, c_hours, c_minutes);
            (c_days == 0 && c_hours == 0 && c_minutes <= 30 && c_minutes > -6) && (
               //  booking.c_minutes = c_minutes,
                bookingToday.push({...booking, c_minutes: c_minutes})
            )
          
      }
        let data_send = "";
        (info.sockets.socket && bookingToday.length > 0) && (
            info.sockets.socket.emit('Booking:Today', bookingToday),
            info.sockets.socket.broadcast.emit('Booking:Today', bookingToday)
        )
   }catch (error) {
      console.log(error)
   }
}
updatePoint = async () =>
{
   try
   {
     await Personnel.updateMany({}, {$set:{point: 30}},{multi: true}).exec()
   }
   catch (error)
   {
      console.log(error)
   }
}
const cronBooking = new CronJob('*/1 * * * *', async function () { //0 */5 * * * *
   await nhacLichHenKhach();
   await thongBaoSinhNhatKhach();
}, null, true, 'Asia/Ho_Chi_Minh');

const cronUpdatePoint = new CronJob('0 0 1 * *', async function () { //0 */5 * * * *
    await updatePoint();
 }, null, true, 'Asia/Ho_Chi_Minh');

module.exports = {
    infoSocke(socket, io){
        info.sockets = [socket, io];
    }
}

