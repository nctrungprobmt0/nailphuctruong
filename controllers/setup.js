const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const _ = require('lodash');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
const moment = require('moment');
const User = require('../models/user');
// CREATES A NEW USER
exports.setup = async (req, res, next) => {
    try {
        let today = moment();
        newUser = new User();
		newUser.fullname = 'admin',
		newUser.username = 'admin',
		newUser.password = newUser.generateHash('123456'),
		newUser.status = 1
        newUser.created = moment(today).format()
        let id = await newUser.save()
        console.log(id)
	    return res.json({
            isSuccess : false
        })

    //   res.status(201).json({ message: 'Successfully deleted' });
    } catch (err) {
      next(err);
    }
  };

exports.index = async (req, res, next) => {
    try {
      const user = await User.findById(req.params.userId);
      if (!user) {
        return res.status(500).json({ message: 'Invalid user ID' });
      }
      user.remove();
      res.status(201).json({ message: 'Successfully deleted' });
    } catch (err) {
      next(err);
    }
  };
