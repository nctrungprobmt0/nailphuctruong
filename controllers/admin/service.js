const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const _ = require('lodash');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
const moment = require('moment');
const Service = require('../../models/service').module();
const jwt  = require('jsonwebtoken');
const config = require('../../config');
const auth = require('../../middlewares/auth');
const upload = require('./../../uploadMiddleware');
const Resize = require('./../../Resize');
const path = require("path");
const bcrypt = require('bcrypt-nodejs');
// CREATES A NEW USER
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function ValidateUSPhoneNumber(phoneNumber) {
  var regExp = /(^0[0-9]{9,10}$)/;
  return regExp.test(String(phoneNumber));
}
function getRandomInt(max) {
  return Math.floor((Math.random() * Math.floor(max))+100000);
}

router.get('/',auth, function (req, res) {
    
	
	Service.find({},function(errs, service){
		res.render('admin/service/service', {
	        title: 'Danh sách dịch vụ',
	        menu : 'service',
	        service : service,
	        layout: 'layout_admin.hbs'
	    })
	})
		
	
})

router.post('/create-service',auth, function (req, res) {
	let today = moment(),
        newService = new Service();
	(req.body.services != ''  ) ? (
		
		newService.services = req.body.services,
		newService.status = req.body.status,
		newService.queue = 0,
		
	    newService.created = moment(today).format(),
	    newService.save( (err) => {
	    	!err ? (
	    		res.status(200).send()
	    	) : (
	    		res.status(401).send({
		            message : 'Error'
		        })
	    	)
	    })
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
    
})

router.post('/get-services-id',auth, function (req, res) {
	
	Service.findOne({'_id' : req.body.id},function(errs, service){
		(!errs && service) ? (
			res.status(200).send(service)
		) : (
			res.status(401).send({
	            message : 'Error'
	        })
		)
	})
})

router.post('/edit-services-by-id',auth, function (req, res) {
	let today = moment();
	console.log(req.body.status);
	(req.body.services != '' ) ? (
		Service.updateOne({'_id' : req.body.id},
			{$set: 
                {
                    "services": req.body.services,
                    "status": req.body.status
                }
            }, function(err, newUser) {
            	res.status(200).send()
		})
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})

router.post('/remove-service-by-id',auth, function (req, res) {
	let today = moment();
	
	(req.body.id != '' ) ? (
		Service.remove({'_id' : req.body.id}, function(err, newUser) {
            	res.status(200).send()
		})
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})
router.post('/home/confirm-Service-by-id',auth, function (req, res) {
	let today = moment();
	
	(req.body.id != '' ) ? (
		Service.updateOne({'_id' : req.body.id},
			{$set: 
                {
                    "status": 2
                }
            }, function(err, newUser) {
            	res.status(200).send()
		})
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})

router.post('/home/create-Service-admin',auth, function (req, res) {
	let today = moment(),
        newService = new Service(),token;
	(req.body.services != '' && req.body.time_book != '' && req.body.date_book != '' && req.body.fullname != '' && req.body.telephone != '') ? (
		token = _.replace(bcrypt.hashSync(new Date(), bcrypt.genSaltSync(8), null),'?','_'),
		req.session.token_crt = token,
		newService.services = req.body.services,
		newService.time_book = req.body.time_book,
		newService.date_book = req.body.date_book,
		newService.fullname = req.body.fullname,
		newService.telephone = req.body.telephone,
		newService.status = 0,
		newService.queue = req.body.queue,
		newService.note = '',
	    newService.created = moment(today).format(),
	    newService.save( (err) => {
	    	!err ? (
	    		res.status(200).send({
	        		token: ''
	    		})
	    	) : (
	    		res.status(401).send({
		            message : 'Error',
		            token: token
		        })
	    	)
	    })
	) : (
		res.status(401).send({
            message : 'Error',token: token
        })
	)
    
})

module.exports = router;