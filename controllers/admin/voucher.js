const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const _ = require('lodash');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
const moment = require('moment');
const Booking = require('../../models/booking').module();
const jwt  = require('jsonwebtoken');
const config = require('../../config');
const auth = require('../../middlewares/auth');
const upload = require('./../../uploadMiddleware');
const Resize = require('./../../Resize');
const path = require("path");
const bcrypt = require('bcrypt-nodejs');
const Voucher = require('../../models/voucher').module();
const Historypoint = require('../../models/history-point').module();
// CREATES A NEW USER
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function ValidateUSPhoneNumber(phoneNumber) {
  var regExp = /(^0[0-9]{9,10}$)/;
  return regExp.test(String(phoneNumber));
}
function getRandomInt(max) {
  return Math.floor((Math.random() * Math.floor(max))+100000);
}


router.get('/',auth, function (req, res) {
    Voucher.find({}).populate('Voucher',{'fullname' : 1,'telephone' : 1}).exec(function(errs, voucher) {
		res.render('admin/voucher/voucher', {
	        title: 'Tích Điểm & Voucher',
	        voucher : voucher,
	        menu : 'voucher',
	        layout: 'layout_admin.hbs'
	    })
	})
})

router.post('/create-voucher',auth, function (req, res) {
	let today = moment(),
        newVoucher = new Voucher();

	(req.body.name != '' && req.body.number != '' && req.body.point != '' && req.body.start_date != '' && req.body.end_date != '') ? (
		Voucher.findOne({'telephone' : req.body.telephone},function(errs, voucher){
			
			newVoucher.name = req.body.name,
			newVoucher.number = req.body.number,
			newVoucher.start_date = moment(req.body.start_date, 'DD/MM/YYYY').toDate(),
			newVoucher.end_date = moment(req.body.end_date, 'DD/MM/YYYY').toDate(),
			newVoucher.status = 0,
			newVoucher.point = req.body.point,
		    newVoucher.created = moment(today).format(),
		    newVoucher.save( (err) => {

		    	!err ? (
		    		res.status(200).send()
		    	) : (
		    		res.status(401).send({
			            message : 'Error'
			        })
		    	)
		    })
			
		})
	
	) : (
		res.status(401).send({
            message : 'Errors'
        })
	)
    
})

router.post('/get-voucher-id',auth, function (req, res) {
	
	Voucher.findOne({'_id' : req.body.id},function(errs, voucher){
		(!errs && voucher) ? (
			res.status(200).send(voucher)
		) : (
			res.status(401).send({
	            message : 'Error'
	        })
		)
	})
})

router.post('/edit-voucher-by-id',auth, function (req, res) {
	let today = moment();
	
	(req.body.name != '' && req.body.number != '' && req.body.start_date != '' && req.body.end_date != '' && req.body.point != '' && req.body.status != '' && req.body.id != '' ) ? (
		Voucher.updateOne({'_id' : req.body.id},
			{$set: 
                {
                    'name' : req.body.name,
	                'number' : req.body.number,
	                'start_date' : moment(req.body.start_date, 'DD/MM/YYYY').toDate(),
	                'end_date' : moment(req.body.end_date, 'DD/MM/YYYY').toDate(),
	                'point' : req.body.point,
	                'status' : req.body.status
                }
            }, function(err, newUser) {
            	res.status(200).send()
		})
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})

router.get('/history',auth, function (req, res) {
    Historypoint
    .find({})
    .populate('member',{'fullname' : 1,'telephone' : 1})
    .populate('user',{'fullname' : 1,'telephone' : 1})
    .exec(function(errs, history) {

		res.render('admin/voucher/history', {
	        title: 'Tích Điểm & Voucher',
	        history : history,
	        menu : 'voucher',
	        layout: 'layout_admin.hbs'
	    })
	})
})
module.exports = router;