const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const _ = require('lodash');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
const moment = require('moment');
const jwt  = require('jsonwebtoken');
const config = require('../../config');
const auth = require('../../middlewares/auth');
const upload = require('./../../uploadMiddleware');
const Resize = require('./../../Resize');
const path = require("path");
const bcrypt = require('bcrypt-nodejs');
const Personnel = require('../../models/personnel').module();
// CREATES A NEW USER
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function ValidateUSPhoneNumber(phoneNumber) {
  var regExp = /(^0[0-9]{9,10}$)/;
  return regExp.test(String(phoneNumber));
}
function getRandomInt(max) {
  return Math.floor((Math.random() * Math.floor(max))+100000);
}

router.get('/', async function (req, res) {
	let personnel = await Personnel.find({}).sort({rate_num:-1})
	personnel = _.sortBy(personnel, o =>  parseFloat(o.rate_avg) <= 5)
	
	res.render('admin/personnel/personnel', {
		title: 'Danh sách nhân sự',
		menu : 'personnel',
		personnel : personnel,
		layout: 'layout_admin.hbs'
	})
})

router.post('/create-personnel',auth, function (req, res) {
	let today = moment(),
        newPersonnel = new Personnel();
	(req.body.fullname != '' && req.body.telephone != '' && req.body.birthday != '' && req.body.address != '' && req.body.salary != '' && req.body.date_work != ''  ) ? (
		
		newPersonnel.fullname = req.body.fullname,
		newPersonnel.telephone = req.body.telephone,
		newPersonnel.birthday = req.body.birthday,
		newPersonnel.address = req.body.address,
		newPersonnel.salary = req.body.salary,
		newPersonnel.status = 0,
		newPersonnel.date_work = req.body.date_work,
	    newPersonnel.created = moment(today).format(),
	    newPersonnel.save( (err) => {
	    	!err ? (
	    		res.status(200).send()
	    	) : (
	    		res.status(401).send({
		            message : 'Error'
		        })
	    	)
	    })
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
    
})
router.post('/advance-salary',auth, async function (req, res) {
	try {
		if(req.body.salary == "" || isNaN(req.body.salary)) return res.status(401).send({ message : 'Error field' })
		let user = await Personnel.findById(req.body._id).exec()
		if(!user) return res.status(401).send({ message : 'Error User' })
		user.advance_salary = parseFloat(req.body.salary)
		await user.save()
		res.status(200).send()
	} catch (error) {
		res.status(401).send({
			message : 'Error'
		})
	}
    
})
router.post('/advance-point',auth, async function (req, res) {
	try {
		if(req.body.point == "" || isNaN(req.body.point)) return res.status(401).send({ message : 'Error field' })
		let user = await Personnel.findById(req.body._id).exec()
		if(!user) return res.status(401).send({ message : 'Error User' })
		user.point = parseFloat(user.point) + parseFloat(req.body.point)
		await user.save()
		res.status(200).send()
	} catch (error) {
		res.status(401).send({
			message : 'Error'
		})
	}
    
})

router.post('/get-personnel-id',auth, function (req, res) {
	
	Personnel.findOne({'_id' : req.body.id},function(errs, personnel){
		(!errs && personnel) ? (
			res.status(200).send(personnel)
		) : (
			res.status(401).send({
	            message : 'Error'
	        })
		)
	})
})

router.post('/edit-personnel-by-id',auth, function (req, res) {
	let today = moment();
	
	(req.body.fullname != '' && req.body.telephone != '' && req.body.birthday != '' && req.body.address != '' && req.body.salary != '' && req.body.date_work != '' && req.body.id != '' ) ? (
		Personnel.updateOne({'_id' : req.body.id},
			{$set: 
                {
                    'fullname' : req.body.fullname,
	                'telephone' : req.body.telephone,
	                'birthday' : req.body.birthday,
	                'address' : req.body.address,
	                'salary' : req.body.salary,
	                'date_work' : req.body.date_work
                }
            }, function(err, newUser) {
            	res.status(200).send()
		})
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})



router.post('/remove-personnel-by-id',auth, function (req, res) {
	let today = moment();
	
	(req.body.id != '' ) ? (
		Personnel.deleteOne({'_id' : req.body.id}, function(err, newUser) {
            	res.status(200).send()
		})
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})

module.exports = router;