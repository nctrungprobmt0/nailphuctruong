const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const _ = require('lodash');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
const moment = require('moment');
const jwt  = require('jsonwebtoken');
const config = require('../../config');
const auth = require('../../middlewares/auth');
const upload = require('./../../uploadMiddleware');
const Resize = require('./../../Resize');
const path = require("path");
const bcrypt = require('bcrypt-nodejs');
const Personnel = require('../../models/personnel').module();
const User = require('../../models/user');
const Booking = require('../../models/booking').module();
// CREATES A NEW USER
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function ValidateUSPhoneNumber(phoneNumber) {
  var regExp = /(^0[0-9]{9,10}$)/;
  return regExp.test(String(phoneNumber));
}
function getRandomInt(max) {
  return Math.floor((Math.random() * Math.floor(max))+100000);
}

router.get('/',auth, function (req, res) {
	Booking.find({'$and' : [{'status' : 2}]}).populate('personnel',{'fullname' : 1,'telephone' : 1}).exec(function(err, booking) {
		res.render('admin/service-evaluation/service-evaluation', {
	        title: 'Thống kê',
	        menu : 'service-evaluation',
	        booking : booking,
	        layout: 'layout_admin.hbs'
	    })
	})
})

router.get('/:id',auth, function (req, res) {
	Booking.find({'$and' : [{'status' : 2},{'personnel' : req.params.id}]}).populate('personnel',{'fullname' : 1,'telephone' : 1}).exec(function(err, booking) {
		res.render('admin/service-evaluation/service-evaluation', {
	        title: 'Thống kê',
	        menu : 'service-evaluation',
	        booking : booking,
	        layout: 'layout_admin.hbs'
	    })
	})
})

router.post('/create-user',auth, function (req, res) {
	let today = moment(),
        newUser = new User();
	(req.body.fullname != '' && req.body.username != '' && req.body.password != '' && req.body.status != '' ) ? (
		
		newUser.fullname = req.body.fullname,
		newUser.username = _.toLower(_.trim(req.body.username)),
		newUser.password = newUser.generateHash(req.body.password),
		newUser.status = req.body.status,
	    newUser.created = moment(today).format(),
	    newUser.save( (err) => {
	    	!err ? (
	    		res.status(200).send()
	    	) : (
	    		res.status(401).send({
		            message : 'Error'
		        })
	    	)
	    })
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
    
})

router.post('/get-user-id',auth, function (req, res) {
	
	User.findOne({'_id' : req.body.id},function(errs, personnel){
		(!errs && personnel) ? (
			res.status(200).send(personnel)
		) : (
			res.status(401).send({
	            message : 'Error'
	        })
		)
	})
})

router.post('/edit-user-by-id',auth, function (req, res) {
	let today = moment();
	
	(req.body.fullname != '' && req.body.username != '' && req.body.status != '' ) ? (
		User.updateOne({'_id' : req.body.id},
			{$set: 
                {
                    'fullname' : req.body.fullname,
	                'username' : req.body.username,
	                'status' : req.body.status
                }
            }, function(err, newUser) {
            	(req.body.password) && (
					User.updateOne({'_id' : req.body.id},
						{$set: 
			                {
			                    'password' : User.generateHash(req.body.password)
			                }
			            }, function(err, newUser) {
			            	res.status(200).send()
					})
				);
            	res.status(200).send()
		})
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})



router.post('/remove-user-by-id',auth, function (req, res) {
	let today = moment();
	
	(req.body.id != '' ) ? (
		User.updateOne({'_id' : req.body.id},
			{$set: 
                {
                    'status' : 8
                }
            }, function(err, newUser) {
            	res.status(200).send()
		})
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})

module.exports = router;