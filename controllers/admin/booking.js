const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const _ = require('lodash');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
const moment = require('moment');
const Booking = require('../../models/booking').module();
const jwt  = require('jsonwebtoken');
const config = require('../../config');
const auth = require('../../middlewares/auth');
const upload = require('./../../uploadMiddleware');
const Resize = require('./../../Resize');
const path = require("path");
const bcrypt = require('bcrypt-nodejs');
const Service = require('../../models/service').module();
const Personnel = require('../../models/personnel').module();
const Member = require('../../models/member').module();
// CREATES A NEW USER



function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function ValidateUSPhoneNumber(phoneNumber) {
  var regExp = /(^0[0-9]{9,10}$)/;
  return regExp.test(String(phoneNumber));
}
function getRandomInt(max) {
  return Math.floor((Math.random() * Math.floor(max))+100000);
}

router.get('/',auth, function (req, res) {
    var token = _.replace(bcrypt.hashSync(new Date(), bcrypt.genSaltSync(8), null),'?','_');
	req.session.token_crt = token;	
	Personnel.find({},function(errs, personnel){
		Service.find({},function(errs, service){
			Booking.find({'$and' : [{'status' : 0},{'queue' : 0}]}).populate('personnel',{'fullname' : 1,'telephone' : 1}).exec(function(err, booking_pending) {
				Booking.find({'$and' : [{'status' : 0},{'queue' : 1}]}).populate('personnel',{'fullname' : 1,'telephone' : 1}).exec(function(errs, booking) {
					
					res.render('admin/booking/booking', {
				        title: 'Danh sách đặt lịch',
				        token : token,
				        menu : 'booking',
				        booking_pending : booking_pending,
				        booking : booking,
				        service : service,
				        personnel : personnel,
				        layout: 'layout_admin.hbs'
				    })
				})
				
			}) 
		})
	})
})

router.get('/statistical',auth, function (req, res) {
    
	Booking.find({'queue' : 1},function(errs, booking){
		res.render('admin/booking/statistical', {
	        title: 'Danh sách đặt lịch',
	        booking : booking,
	        menu : 'booking',
	        layout: 'layout_admin.hbs'
	    })
	})
		
	
})

router.post('/create-booking',auth, function (req, res) {
	let today = moment(),
        newBooking = new Booking(),token;
	(req.body.services != '' && req.body.time_book != '' && req.body.date_book != '' && req.body.fullname != '' && req.body.telephone != '' && req.body.token == req.session.token_crt ) ? (
		token = _.replace(bcrypt.hashSync(new Date(), bcrypt.genSaltSync(8), null),'?','_'),
		req.session.token_crt = token,
		newBooking.services = req.body.services,
		newBooking.time_book = req.body.time_book,
		newBooking.date_book = req.body.date_book,
		newBooking.fullname = req.body.fullname,
		newBooking.telephone = req.body.telephone,
		newBooking.status = 0,
		newBooking.queue = 0,
		newBooking.online = 0,
		newBooking.note = '',
	    newBooking.created = moment(today).format(),
	    newBooking.personnel = req.body.personnel ,
	    newBooking.save( (err) => {
	    	!err ? (
	    		res.status(200).send({
	        		token: token
	    		})
	    	) : (
	    		res.status(401).send({
		            message : 'Error',
		            token: token
		        })
	    	)
	    })
	) : (
		res.status(401).send({
            message : 'Error',token: token
        })
	)
    
})

router.post('/get-booking-id',auth, function (req, res) {
	
	Booking.findOne({'_id' : req.body.id},function(errs, booking){
		(!errs && booking) ? (
			res.status(200).send(booking)
		) : (
			res.status(401).send({
	            message : 'Error'
	        })
		)
	})
})

router.post('/edit-booking-by-id',auth, function (req, res) {
	let today = moment();
	let newMember = new Member();
	(req.body.services != '' && req.body.time_book != '' && req.body.date_book != '' && req.body.fullname != '' && req.body.telephone != '' && req.body.id != '' ) ? (
		Booking.updateOne({'_id' : req.body.id},
			{$set: 
                {
                    "services": req.body.services,
                    "time_book": req.body.time_book,
                    "date_book": req.body.date_book,
                    "fullname": req.body.fullname,
                    "telephone": _.trim(req.body.telephone),
                    "queue" : req.body.queue,
                    "personnel" : req.body.personnel,
                    "online" : 0
                }
            }, function(err, newUser) {
            	(parseInt(req.body.queue) == 1) ? (
            		Member.findOne({'telephone' : req.body.telephone},function(errs, member){
						(!errs && !member) ? (
							newMember.fullname = req.body.fullname,
							newMember.telephone = _.trim(req.body.telephone),
							newMember.birthday = '',
							newMember.address = '',
							newMember.status = 0,
							newMember.point = 0,
						    newMember.created = moment(today).format(),
						    newMember.save( (err) => {
						    	res.status(200).send()
						    })
						) : res.status(200).send()
					})
            	) : res.status(200).send()
		})
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})

router.post('/cancel-booking-by-id',auth, function (req, res) {
	let today = moment();
	
	(req.body.id != '' ) ? (
		Booking.updateOne({'_id' : req.body.id},
			{$set: 
                {
                    "status": 8
                }
            }, function(err, newUser) {
            	res.status(200).send()
		})
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})
router.post('/confirm-booking-by-id',auth, async function (req, res) {
	try {
		let {id, amount} = req.body;
		let booking = await Booking.findById(id)
		if(booking){
			booking.bill = amount;
			booking.status = 2;
			await booking.save()

			let member = await Member.findOne({'telephone': booking.telephone})
			if(member){
				member.bill = parseFloat(member.bill) + parseFloat(amount)
				await member.save()
			}
			return res.status(200).json({
				message : 'Success',
				isSuccess : true
			});
		}else{
			return res.status(500).json({
				message : 'Booking not found',
				isSuccess : false
			});
		}
		
	} catch (error) {
		console.log(error)
		return res.status(500).json({
			message : 'Sorry, the server is unable to respond.',
			isSuccess : false
		});
	}
})

router.post('/create-booking-admin',auth, function (req, res) {
	let today = moment(),
        newBooking = new Booking(),token;
	(req.body.services != '' && req.body.time_book != '' && req.body.date_book != '' && req.body.fullname != '' && req.body.telephone != '') ? (
		token = _.replace(bcrypt.hashSync(new Date(), bcrypt.genSaltSync(8), null),'?','_'),
		req.session.token_crt = token,
		newBooking.services = req.body.services,
		newBooking.time_book = req.body.time_book,
		newBooking.date_book = req.body.date_book,
		newBooking.fullname = req.body.fullname,
		newBooking.telephone = req.body.telephone,
		newBooking.status = 0,
		newBooking.queue = req.body.queue,
		newBooking.note = '',
	    newBooking.created = moment(today).format(),
	    newBooking.personnel = req.body.personnel,
	    newBooking.save( (err) => {
	    	
	    	!err ? (
	    		res.status(200).send({
	        		token: ''
	    		})
	    	) : (
	    		res.status(401).send({
		            message : 'Error',
		            token: token
		        })
	    	)
	    })
	) : (
		res.status(401).send({
            message : 'Error',token: token
        })
	)
})

router.get('/update-status-enlable/:id',auth, function (req, res) {
	Booking.updateOne({'_id' : req.params.id},
		{$set: 
            {
                "online": 1
            }
        }, function(err, newUser) {
        	res.redirect('/administrator/booking')
	})
})

router.get('/update-status-disable/:id',auth, function (req, res) {
	Booking.updateOne({'_id' : req.params.id},
		{$set: 
            {
                "online": 0
            }
        }, function(err, newUser) {
        	res.redirect('/administrator/booking')
	})
})

router.get('/get-online', function (req, res) {
	
	Booking.find({'$and' : [{'online' : 1},{'status' : 0}]} ).populate('personnel',{'fullname' : 1,'telephone' : 1}).exec(function(errs, booking) {
	
		(!errs && booking) ? (
			res.status(200).send(booking)
		) : (
			res.status(401).send({
	            message : 'Error'
	        })
		)
	})
})

/*router.get('/get-online',function (req, res) {
	
	Booking.find({'$and' : [{'online' : 1},{'status' : 0}]} ).populate('personnel',{'fullname' : 1,'telephone' : 1}).exec(function(errs, booking) {
		var array = [];
		if (!errs && booking)
		{
			for (var i = booking.length - 1; i >= 0; i--) {
				array.push({
					'fullname' : booking[i].fullname,
					'telephone' : booking[i].telephone,
					'_id' : booking[i]._id,
					'personnel_id' : booking[i].personnel._id
				}) 
			}
			res.status(200).send({
	            'data' : array
	        })
		}
		else
		{
			res.status(200).send({
	            'data' : array
	        })
		}
		
	})
})*/
module.exports = router;