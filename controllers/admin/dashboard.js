const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const _ = require('lodash');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
const moment = require('moment');
const User = require('../../models/user');
const Member = require('../../models/member').module();
const Booking = require('../../models/booking').module();
process.env.TZ = 'Asia/Ho_Chi_Minh'
// CREATES A NEW USER
exports.thongBaoSinhNhat = async () =>
{
   try
   {
      let members = await Member.find({})
      let today = moment()
      today =  moment(today).format()
      console.log(today)
      var year = moment().year();
      let toDayBirthday= [];
      let upcomingBirthdays = [];
      for (const member of members)
      {
         var parts = member.birthday.split("/");
         var parts_date = `${year}-${parseInt(parts[1], 10)}-${parseInt(parts[0], 10)}`
         var dt = new Date(parts_date);
         let birthday = moment(dt);
         let c_days = birthday.diff(today, 'days');
         let c_hours = birthday.subtract(c_days, 'days').diff(today, 'hours');
         let c_minutes = birthday.subtract(c_hours, 'hours').diff(today, 'minutes');
         console.log(c_days, c_hours, c_minutes)
         if(c_days >= 0 && c_days <= 7 && c_hours <= 0){
             member.birthday = `${parseInt(parts[0], 10)}/${parseInt(parts[1], 10)}/${year}`
            toDayBirthday.push(member)
         }
         
      }
       return toDayBirthday
   }
   catch (error)
   {
      console.log(error)
   }
}

exports.nhacLichHen = async () =>
{
   try {
      let bookings = await Booking.find({status: 0})
      let today = moment()
      today =  moment(today).format()
      let bookingToday = []
      for (const booking of bookings) {
         var parts = booking.date_book.split("/");
         var dt = new Date(parseInt(parts[2], 10), parseInt(parts[1], 10) - 1, parseInt(parts[0], 10));
         let days = moment(dt).format('YYYY-MM-DD');
         let time_book = days + " " + booking.time_book + ':00';
         let dateTimeBook = moment(new Date(time_book)).format();
         dateTimeBook = moment(dateTimeBook)

         let c_days = dateTimeBook.diff(today, 'days');
         let c_hours = dateTimeBook.subtract(c_days, 'days').diff(today, 'hours');
         let c_minutes = dateTimeBook.subtract(c_hours, 'hours').diff(today, 'minutes');
            (c_days == 0 && c_hours == 0 && c_minutes <= 30 && c_minutes > -6) && (
                bookingToday.push(booking)
            )
          
      }
       return bookingToday
   }catch (error) {
      console.log(error)
   }
}
exports.index = async (req, res, next) => {
    try {
      let thongBaoSinhNhat = await  this.thongBaoSinhNhat()
      let nhacLichHen = await this.nhacLichHen()
	    res.render('admin/dashboard/dashboard', {
	        title: 'Tổng quan',
	        menu : 'dashboard',
          layout: 'layout_admin.hbs',
          nhacLichHen: nhacLichHen,
          thongBaoSinhNhat: thongBaoSinhNhat
	    })
    } catch (err) {
      next(err);
    }
  };

