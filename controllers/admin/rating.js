const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const _ = require('lodash');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
const moment = require('moment');
const Booking = require('../../models/booking').module();
const jwt  = require('jsonwebtoken');
const config = require('../../config');
const auth = require('../../middlewares/auth');
const upload = require('./../../uploadMiddleware');
const Resize = require('./../../Resize');
const path = require("path");
const bcrypt = require('bcrypt-nodejs');
const Service = require('../../models/service').module();
const Rating = require('../../models/rating').module();
// CREATES A NEW USER
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function ValidateUSPhoneNumber(phoneNumber) {
  var regExp = /(^0[0-9]{9,10}$)/;
  return regExp.test(String(phoneNumber));
}
function getRandomInt(max) {
  return Math.floor((Math.random() * Math.floor(max))+100000);
}

router.get('/',auth, function (req, res) {
    
    Rating.find({}).populate('booking_id',{'fullname' : 1,'telephone' : 1}).populate('personnel_id',{'fullname' : 1,'telephone' : 1}).exec(function(errs, rating) {
	//Rating.find({},function(errs, rating){
		
		res.render('admin/rating/rating', {
	        title: 'Danh sách đánh giá dịch vụ',
	        menu : 'rating',
	        rating : rating,
	        layout: 'layout_admin.hbs'
	    })	
	})
})



module.exports = router;