const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const _ = require('lodash');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
const moment = require('moment');
const jwt  = require('jsonwebtoken');
const config = require('../../config');
const auth = require('../../middlewares/auth');
const upload = require('./../../uploadMiddleware');
const Resize = require('./../../Resize');
const path = require("path");
const bcrypt = require('bcrypt-nodejs');
const Member = require('../../models/member').module();
const Voucher = require('../../models/voucher').module();
const Historyvoucher = require('../../models/history-voucher').module();
const Historypoint = require('../../models/history-point').module();
process.env.TZ = 'Asia/Ho_Chi_Minh'
// CREATES A NEW USER
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function ValidateUSPhoneNumber(phoneNumber) {
  var regExp = /(^0[0-9]{9,10}$)/;
  return regExp.test(String(phoneNumber));
}
function getRandomInt(max) {
  return Math.floor((Math.random() * Math.floor(max))+100000);
}
function save_history_point(member,point,type,user,description,callback){
	let newHistorypoint = new Historypoint();
	let today = moment();
	newHistorypoint.member = member,
	newHistorypoint.point = point,
	newHistorypoint.type = type,
	newHistorypoint.user = user,
	newHistorypoint.description = description,
    newHistorypoint.created = moment(today).format(),
    newHistorypoint.save( (err) => {
    	!err ? (
    		callback(true)
    	) : (
    		callback(false)
    	)
    })
}

function save_history_voucher(user,voucher_id,user,callback){
	let newHistoryvoucher = new Historyvoucher();
	let today = moment();
	newHistoryvoucher.member = user,
	newHistoryvoucher.voucher = voucher_id,
	newHistoryvoucher.user = user,
    newHistoryvoucher.created = moment(today).format(),
    newHistoryvoucher.save( (err) => {
    	!err ? (
    		callback(true)
    	) : (
    		callback(false)
    	)
    })
}

thongBaoSinhNhat = async () =>
{
   try
   {
      let members = await Member.find({})
      let today = moment()
      today =  moment(today).format()
      console.log(today)
      var year = moment().year();
      let toDayBirthday= [];
      let upcomingBirthdays = [];
      for (let member of members)
      {
		  member = JSON.parse(JSON.stringify(member))
         var parts = member.birthday.split("/");
         var parts_date = `${year}-${parseInt(parts[1], 10)}-${parseInt(parts[0], 10)}`
         var dt = new Date(parts_date);
         let birthday = moment(dt);
         let c_days = birthday.diff(today, 'days');
         let c_hours = birthday.subtract(c_days, 'days').diff(today, 'hours');
         let c_minutes = birthday.subtract(c_hours, 'hours').diff(today, 'minutes');
		 console.log(c_days, c_hours, c_minutes)
		 let datamember = {
			 c_days: 0,
			 c_hours: 0,
			 c_minutes: 0
		 }
        //  if(c_days >= 0 && c_days <= 7){
			 member.birthday = member.birthday ?  `${parseInt(parts[0], 10)}/${parseInt(parts[1], 10)}/${year}` : ''
			 let day = parseInt(c_hours) > 0 ? parseInt(c_days) + 1: c_days
			 let newArr = {
				 class: parseInt(day) == 0 ? 'joe' : (
					 parseInt(day) > 0 && parseInt(day) <= 5 ? 'joe1' : ''
				 ),
				c_days: day < 0 ? 1000 : day,
				c_hours: c_hours,
				c_minutes: c_minutes,
				 ...member
			 }
			//  console.log(newArr)
            toDayBirthday.push(newArr)
        //  }
         
      }
       return toDayBirthday
   }
   catch (error)
   {
      console.log(error)
   }
}


router.get('/',auth, async function (req, res) {
	let today = moment();
	let member = await thongBaoSinhNhat();
	
	member = _.orderBy(member, ['c_days'],['asc']);
	// console.log(birthday)
	// let member = await Member.find({}).sort({point:-1})



	let voucher = await Voucher.find({'$and' : [
		{'status' : 0},
		{'number' : {'$gt' : 0}},
		{'start_date' : {'$lt' : new Date()}},
		{'end_date' : {'$gt' : new Date()}}
		]}).exec()
		res.render('admin/member/member', {
			title: 'Danh sách khách hàng',
			menu : 'member',
			member : member,
			voucher : voucher,
			layout: 'layout_admin.hbs'
		})
})

router.post('/create-member',auth, function (req, res) {
	let today = moment(),
        newMember = new Member();
	(req.body.fullname != '' && req.body.telephone != '' ) ? (
		Member.findOne({'telephone' : req.body.telephone},function(errs, member){
			(!errs && !member) ? (
				newMember.fullname = req.body.fullname,
				newMember.telephone = req.body.telephone,
				newMember.birthday = req.body.birthday,
				newMember.address = req.body.address,
				newMember.status = 0,
				newMember.point = 0,
			    newMember.created = moment(today).format(),
			    newMember.save( (err) => {
			    	!err ? (
			    		res.status(200).send()
			    	) : (
			    		res.status(401).send({
				            message : 'Error'
				        })
			    	)
			    })
			) : (
				res.status(401).send({
		            message : 'Số điện thoại đã tồn tại trong hệ thống',
		        })
			)
		})


				
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
    
})

router.post('/get-member-id',auth, function (req, res) {
	
	Member.findOne({'_id' : req.body.id},function(errs, member){
		(!errs && member) ? (
			res.status(200).send(member)
		) : (
			res.status(401).send({
	            message : 'Error'
	        })
		)
	})
})

router.post('/edit-member-by-id',auth, function (req, res) {
	let today = moment();
	
	(req.body.fullname != '' && req.body.telephone != '' && req.body.birthday != '' && req.body.address != '' && req.body.salary != '' && req.body.date_work != '' && req.body.id != '' ) ? (
		Member.findOne({'$and' : [{'telephone' : req.body.telephone},{'_id' : { '$ne' : req.body.id }}]} ,function(errs, member){
			
			(!errs && !member) ? (
				Member.updateOne({'_id' : req.body.id},
					{$set: 
		                {
		                    'fullname' : req.body.fullname,
			                'telephone' : req.body.telephone,
			                'birthday' : req.body.birthday,
			                'address' : req.body.address
		                }
		            }, function(err, newUser) {
		            	res.status(200).send()
				})
			) : (
				res.status(401).send({
		            message : 'Số điện thoại đã tồn tại trong hệ thống',
		        })
			)
		})
		
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})

router.post('/add-point',auth, function (req, res) {
	let today = moment();
	
	(req.body.point != '' && req.body.id != '' ) ? (
		Member.findOne({'_id' : req.body.id} ,function(errs, member){
			(!errs && member) ? (
				Member.updateOne({'_id' : req.body.id},
				{$set: 
	                {
	                    'point' : parseFloat(member.point)+parseFloat(req.body.point)
	                }
	            }, function(err, newUser) {
	            	save_history_point(req.body.id,req.body.point,'+',req.user._id,'Tích điểm',function(cb){
	            		res.status(200).send()
	            	})
				})
			) : (
				res.status(401).send({
		            message : 'Error'
		        })
			)
		})
		
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})

router.post('/created-voucher',auth, function (req, res) {
	let today = moment();
	
	(req.body.voucher != '' && req.body.id != '') ? (
		Member.findOne({'_id' : req.body.id} ,function(errs, member){
			(!errs && member) ? (
				Voucher.findOne({'$and' : [{'status' : 0},{'_id' : req.body.voucher}]} ,function(errss, voucher){
					(!errss && voucher) ? (
						(parseFloat(member.point) >= parseFloat(voucher.point)) ? (
							Member.updateOne({'_id' : req.body.id},
							{$set: 
				                {
				                    'point' : parseFloat(member.point)-parseFloat(voucher.point)
				                }
				            }, function(err, newUser) {
				            	save_history_point(req.body.id,voucher.point,'-',req.user._id,'Tạo voucher '+voucher.name,function(cb){
				            		save_history_voucher(req.body.id,voucher._id,req.user._id,function(cb){
				            			Voucher.updateOne({'_id' : voucher._id},
											{$set: 
								                {
									                'number' : parseInt(voucher.number) - 1 
								                }
								            }, function(err, newUser) {
								            	res.status(200).send()
										})
				            			
				            		})
				            		
				            	})
							})
						) : (
							res.status(401).send({
					            message : 'Số điểm không đủ để tạo voucher'
					        })
						)
					) : (
						res.status(401).send({
				            message : 'This voucher does not exist'
				        })
					)
				})

			) : (
				res.status(401).send({
		            message : 'Error'
		        })
			)
		})
		
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})


router.post('/remove-member-by-id',auth, function (req, res) {
	let today = moment();
	
	(req.body.id != '' ) ? (
		Member.deleteOne({'_id' : req.body.id}, function(err, newUser) {
            	res.status(200).send()
		})
	) : (
		res.status(401).send({
            message : 'Error'
        })
	)
})

module.exports = router;