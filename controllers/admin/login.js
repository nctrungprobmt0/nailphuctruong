const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const _ = require('lodash');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
const moment = require('moment');
const Booking = require('../../models/booking').module();
const jwt  = require('jsonwebtoken');
const config = require('../../config');
const auth = require('../../middlewares/auth');
const upload = require('./../../uploadMiddleware');
const Resize = require('./../../Resize');
const path = require("path");
const bcrypt = require('bcrypt-nodejs');
const User = require('../../models/user');
// CREATES A NEW USER
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function ValidateUSPhoneNumber(phoneNumber) {
  var regExp = /(^0[0-9]{9,10}$)/;
  return regExp.test(String(phoneNumber));
}
function getRandomInt(max) {
  return Math.floor((Math.random() * Math.floor(max))+100000);
}

router.get('/login', function (req, res) {
    var token = _.replace(bcrypt.hashSync(new Date(), bcrypt.genSaltSync(8), null),'?','_');
	req.session.token_crt = token;	
    res.render('admin/login', {
        title: 'Login',
        token : token,
        error : false
    });
})

router.post('/login', function (req, res) {
    var token = _.replace(bcrypt.hashSync(new Date(), bcrypt.genSaltSync(8), null),'?','_');
	req.session.token_crt = token;	

	User.findOne({ 'username': req.body.username }, function(err, user) {
		user ? (
			!user.validPassword(req.body.password) ? (
				res.render('admin/login', {
			        title: 'Login',
			        token : token,
			        error : true
			    })
            ) : (
            	req.session.userId = user._id,
				res.redirect('/administrator/dashboard')
            )
		) : (
			res.render('admin/login', {
		        title: 'Login',
		        token : token,
		        error : true
		    })
		)
	})
})

router.get('/logout', function (req, res) {
	delete req.session.userId;
	res.redirect('/administrator/login')
})
module.exports = router;