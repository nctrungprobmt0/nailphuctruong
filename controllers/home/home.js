const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const _ = require('lodash');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
const moment = require('moment');
const Booking = require('../../models/booking').module();
const Member = require('../../models/member').module();
const jwt  = require('jsonwebtoken');
const config = require('../../config');
const path = require("path");
const bcrypt = require('bcrypt-nodejs');
const Service = require('../../models/service').module();
const Rating = require('../../models/rating').module();
const Personnel = require('../../models/personnel').module();
// CREATES A NEW USER
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function ValidateUSPhoneNumber(phoneNumber) {
  var regExp = /(^0[0-9]{9,10}$)/;
  return regExp.test(String(phoneNumber));
}
function getRandomInt(max) {
  return Math.floor((Math.random() * Math.floor(max))+100000);
}


router.get('/', function (req, res) {
    Service.find({'status' : 0},function(errs, service){
	    var token = _.replace(bcrypt.hashSync(new Date(), bcrypt.genSaltSync(8), null),'?','_');
		req.session.token_crt = token;	
	    res.render('home/home', {
	        title: 'Home',
	        token : token,
	        service : service,
	        layout: 'layout_home.hbs'
	    });
	})
})

router.get('/rating', function (req, res) {
    
    res.render('home/rating', {
        title: 'rating',
        layout: 'layout_home.hbs'
    });
	
})


router.get('/profile/:phone', async function (req, res) {
	let user = await Member.findOne({telephone: req.params.phone})
	let booking = await Booking.find({telephone: req.params.phone})
	if(!user) user = null
	if (!booking) booking = null
    res.render('home/profile', {
        title: 'profile',
		layout: 'layout_home.hbs',
		user: user,
		booking: booking
    });
	
})
thongBaoSinhNhat = async () =>
{
   try
   {
      let members = await Member.find({})
      let today = moment()
      today =  moment(today).format()
      var year = moment().year();
      let toDayBirthday= [];
      let upcomingBirthdays = [];
      for (const member of members)
      {
         var parts = member.birthday.split("/");
         var parts_date = `${year}-${parseInt(parts[1], 10)}-${parseInt(parts[0], 10)}`
         var dt = new Date(parts_date);
         let birthday = moment(dt);
         let c_days = birthday.diff(today, 'days');
         let c_hours = birthday.subtract(c_days, 'days').diff(today, 'hours');
         let c_minutes = birthday.subtract(c_hours, 'hours').diff(today, 'minutes');
         if(c_days >= 0 && c_days <= 7){
             member.birthday = `${parseInt(parts[0], 10)}/${parseInt(parts[1], 10)}/${year}`
            toDayBirthday.push(member)
         }

      }
       return toDayBirthday
   }
   catch (error)
   {
      console.log(error)
   }
}
router.get('/gift', async function (req, res) {
	let user = await this.thongBaoSinhNhat()
	
	if(user.length == 0) user = null
    res.render('home/gift', {
        title: 'gift',
		layout: 'layout_home.hbs',
		user: user
    });
	
})
router.post('/home/create-booking', function (req, res) {
	let today = moment(),
        newBooking = new Booking(),token;
	(req.body.services != '' && req.body.time_book != '' && req.body.date_book != '' && req.body.fullname != '' && req.body.telephone != '' && req.body.token == req.session.token_crt ) ? (
		token = _.replace(bcrypt.hashSync(new Date(), bcrypt.genSaltSync(8), null),'?','_'),
		req.session.token_crt = token,
		newBooking.services = req.body.services,
		newBooking.time_book = req.body.time_book,
		newBooking.date_book = req.body.date_book,
		newBooking.fullname = req.body.fullname,
		newBooking.telephone = req.body.telephone,
		newBooking.status = 0,
		newBooking.queue = 0,
		newBooking.online = 0,
		newBooking.note = '',
		newBooking.rating = '',
	    newBooking.created = moment(today).format(),
	    newBooking.save( (err) => {
	    	!err ? (
	    		res.status(200).send({
	        		token: ''
	    		})
	    	) : (
	    		res.status(401).send({
		            message : 'Errorssss',
		            token: token
		        })
	    	)
	    })
	) : (
		res.status(401).send({
            message : 'Errors',token: token
        })
	)
    
})


router.post('/home/create-rating', async function (req, res) {
	let today = moment(), newRating = new Rating();

	try {
		if(req.body.type != '' ){
			newRating.type = req.body.type,
			newRating.description = req.body.description,
			newRating.option = req.body.option == undefined ? [] : req.body.option,
			newRating.status = 0,
			newRating.rate_num = req.body.rate_stars,
			newRating.booking_id = req.body.booking_id,
			newRating.personnel_id = req.body.personnel_id,
			newRating.created = moment(today).format(),
			
			await newRating.save(),
			
			await Booking.updateOne({'_id' : req.body.booking_id}, {$set: { "status": 2, "rating" : req.body.type } })

			let personnel = await Personnel.findById(req.body.personnel_id)
			let rate_num = parseFloat(personnel.rate_num) + 1;
			let rate_avg = (parseFloat(personnel.rate_num)*parseFloat(personnel.rate_avg)+parseFloat(req.body.rate_stars))/parseFloat(rate_num)
			await Personnel.updateMany({_id: personnel._id},{$set:{
				rate_num: rate_num,
				rate_avg: parseFloat(rate_avg).toFixed(2)
			}})
			res.status(200).send({
				message : 'asd'
			})
		}else{
			res.status(401).send({
				message : 'Error'
			})
		}
		
	} catch (error) {
		console.log(error)
		res.status(401).send({
            message : 'Error'
        })
	}
    
})

module.exports = router;