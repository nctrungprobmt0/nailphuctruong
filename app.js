var app = require('express')();

//var db = require('./db');

const cors = require('cors');
const express = require('express');
//const fileUpload = require('express-fileupload');
const bodyParser= require('body-parser')
//app.use(fileUpload());
const hbs = require('express-handlebars');
const config = require('./config');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);

app.use(session({
	secret : "secq%&(w-e@rwqe-rAs@&dfasdfr%^&*)et",
	store: new MongoDBStore({
		uri: config.db,
		collection: 'session'
	}),
	cookie: {
        expires: new Date(Date.now() + 60 * 10000 * 15), 
  		maxAge: 60 * 10000 * 15
    },
	saveUninitialized: true,
	resave: true
}))


app.use(bodyParser.urlencoded({
  extended: true
}));


app.use(bodyParser.json())

app.use('/public', express.static('public'));
app.use('/.well-known', express.static('.well-known'));
app.use(cors());
app.options('*', cors());

app.engine('.hbs',hbs({ 
	defaultLayout: 'default',
	extname: '.hbs',
	helpers: require('./services/helpers').helpers
}))
app.set('view engine', '.hbs')


const CronJob = require('./controllers/cron');

var HomeController = require('./controllers/home/home');
app.use('/', HomeController);

//------------admin
var AdminBookingController = require('./controllers/admin/booking');
app.use('/administrator/booking', AdminBookingController);

var AdminLoginController = require('./controllers/admin/login');
app.use('/administrator', AdminLoginController);

var AdminServiceController = require('./controllers/admin/service');
app.use('/administrator/service', AdminServiceController);

var AdminPersonnelController = require('./controllers/admin/personnel');
app.use('/administrator/personnel', AdminPersonnelController);

var AdminMemberController = require('./controllers/admin/member');
app.use('/administrator/member', AdminMemberController);

var AdminVoucherController = require('./controllers/admin/voucher');
app.use('/administrator/voucher', AdminVoucherController);

var AdminRatingController = require('./controllers/admin/rating');
app.use('/administrator/rating', AdminRatingController);

var AdminMangerAccountController = require('./controllers/admin/manger-account');
app.use('/administrator/manger-account', AdminMangerAccountController);

var AdminServiceEvaluationController = require('./controllers/admin/service-evaluation');
app.use('/administrator/service-evaluation', AdminServiceEvaluationController);


const routes = require('./routes')(app);

module.exports = app;